# pi-hole-helpers

This repo has been forked from the [pi-hole-helpers](https://github.com/Kevin-De-Koninck/pi-hole-helpers/) Github from [@Kevin-De-Koninck](https://github.com/Kevin-De-Koninck/), and has been updated by me to work with the latest pihole-FTL version. <br>
I've also updated some scripts to otpimized them.

This will install a command to add a custom DNS entry. With this, you can now reach your local devices using a readable URL (e.g. http://movie.server) instead of an IP address (http://192.168.1.50).

## Install
On a clean pi-hole, install the helper with the following command:
```bash
# Method 1.
curl -sSL https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/install.sh | bash

# Method 2.
wget -O /tmp/install.sh https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/install.sh
chmod +x /tmp/install.sh
sudo /tmp/install.sh
```

## Usage
```bash
# Add entry.
sudo pihole-helper # Same as 'sudo pihole-helper --add-dns-entry' -> default argument
# Remove entry.
sudo pihole-helper --remove-dns-entry
# List current custom entries.
sudo pihole-helper --list
# View options.
sudo pihole-helper -h
```

## Uninstall
```bash
# Method 1.
curl -sSL https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/uninstall.sh | bash

# Method 2.
wget -O /tmp/uninstall.sh https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/uninstall.sh
chmod +x /tmp/uninstall.sh
sudo /tmp/uninstall.sh
```
