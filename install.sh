#!/bin/bash

# Check if root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root or use sudo..."
  exit 1
fi

# see if pi-hole dir exists
if ! [ -e /opt/pihole/ ]
then
  echo "Directory '/opt/pihole/' does not exist. Please install the latest version of pi-hole first."
  exit 1
fi

# get and save the scripts
wget -O /opt/pihole/whiptail.sh https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/whiptail.sh
wget -O /opt/pihole/pihole-helper.sh https://gitlab.com/Natizyskunk/pi-hole-helpers/raw/master/pihole-helper.sh

# Make 'em executable
chmod +x /opt/pihole/whiptail.sh
chmod +x /opt/pihole/pihole-helper.sh

# Register our custom list
echo "addn-hosts=/etc/pihole/custom-LAN.list" > /etc/dnsmasq.d/02-custom-LAN.conf

# register command
real_user=$(who am i | awk '{print $1}')

if ["$real_user" = "root"]
then
  if [ -f /root/.bash_aliases ]
  then
    echo "" >> /root/.bash_aliases
    echo "# Register pihole-helper to easily add a custom DNS entry" >> /root/.bash_aliases
    echo "alias pihole-helper='/opt/pihole/pihole-helper.sh'" >> /root/.bash_aliases
    echo "" >> /root/.bash_aliases
  else
    echo "" >> /root/.bashrc
    echo "# Register pihole-helper to easily add a custom DNS entry" >> /root/.bashrc
    echo "alias pihole-helper='/opt/pihole/pihole-helper.sh'" >> /root/.bashrc
    echo "" >> /root/.bashrc
  fi

  # reload bashrc
  source /root/.bashrc
else
  if [ -f /home/${real_user}/.bash_aliases ]
  then
    echo "" >> /home/${real_user}/.bash_aliases
    echo "# Register pihole-helper to easily add a custom DNS entry" >> /home/${real_user}/.bash_aliases
    echo "alias pihole-helper='/opt/pihole/pihole-helper.sh'" >> /home/${real_user}/.bash_aliases
    echo "" >> /home/${real_user}/.bash_aliase
  else
    echo "" >> /home/${real_user}/.bashrc
    echo "# Register pihole-helper to easily add a custom DNS entry" >> /home/${real_user}/.bashrc
    echo "alias pihole-helper='/opt/pihole/pihole-helper.sh'" >> /home/${real_user}/.bashrc
    echo "" >> /home/${real_user}/.bashrc
  fi

  # reload bashrc
  source /home/${real_user}.bashrc
fi

# Inform the user
echo "--------------------------------------------------------------------------"
echo ""
echo "PLEASE RUN 'source ~/.bashrc' IN YOUR TERMINAL TO SYNC EVERYTHING."
echo ""
echo "You can now use the 'pihole-helper' command to add custom DNS entries."
echo "Use 'pihole-helper -h' to view your options."
echo ""

exit 1
